// **Iteración #2: Mix Fors**

/* Dado el siguiente javascript usa forof y forin para hacer la media del volumen de todos los sonidos favoritos
que tienen los usuarios.
 */

                            /* CON BASTANTE AYUDA */

// me ayudo un compa porque me quedé pillado en sacar el valor del objeto dentro del objeto, 
// yo pretendía dentro del objeto user.favoritesSounds sacar la propiedad volumen , 
// sumarlos todos y sacar la media pero no he sido capaz

const users = [{
    name: 'Manolo el del bombo',
    favoritesSounds: {
        waves: {
            format: 'mp3',
            volume: 50
        },
        rain: {
            format: 'ogg',
            volume: 60
        },
        firecamp: {
            format: 'mp3',
            volume: 80
        },
    }
},
{
    name: 'Mortadelo',
    favoritesSounds: {
        waves: {
            format: 'mp3',
            volume: 30
        },
        shower: {
            format: 'ogg',
            volume: 55
        },
        train: {
            format: 'mp3',
            volume: 60
        },
    }
},
{
    name: 'Super Lopez',
    favoritesSounds: {
        shower: {
            format: 'mp3',
            volume: 50
        },
        train: {
            format: 'ogg',
            volume: 60
        },
        firecamp: {
            format: 'mp3',
            volume: 80
        },
    }
},
{
    name: 'El culebra',
    favoritesSounds: {
        waves: {
            format: 'mp3',
            volume: 67
        },
        wind: {
            format: 'ogg',
            volume: 35
        },
        firecamp: {
            format: 'mp3',
            volume: 60
        },
    }
},
]

let volumesCounter = 0;
let sumVolumes = 0;

for (user of users) {
    let favoritesSounds = user.favoritesSounds
    // console.log(favoritesSounds)
    for (value in favoritesSounds) {
        for (value_inner in favoritesSounds[value]) {
            // console.log(value_inner) // format y volume
            // console.log(favoritesSounds[value].volume)
            if (value_inner === 'volume') {
                volumesCounter += 1;
                sumVolumes += favoritesSounds[value][value_inner];
            }
        }
    }
}

let average = sumVolumes / volumesCounter;
console.log(average);
