/* **Iteración #4: Métodos findArrayIndex**

Crea una función llamada `findArrayIndex` que reciba como parametros un array de textos
y un texto y devuelve la posición del array cuando el valor del array
sea igual al valor del texto que enviaste como parametro. Haz varios ejemplos y compruebalos.

Sugerencia de función: */
const animales = ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote']

function findArrayIndex(array, text) {
		let pos = array.indexOf(text);
		// return pos;
		console.log(pos);

}
findArrayIndex(animales, 'Mosquito');
findArrayIndex(animales, 'Caracol');
findArrayIndex(animales, 'Ajolote');
findArrayIndex(animales, 'Salamandra');
